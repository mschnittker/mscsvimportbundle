<?php

namespace MS\CsvImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function listAction()
    {
        $results = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('MSCsvImportBundle:Data')
                    ->getAllResults();

        return $this->render('MSCsvImportBundle:Default:list.html.twig', [
            'results' => $results
        ]);
    }
}
