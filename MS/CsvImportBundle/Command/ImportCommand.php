<?php

namespace MS\CsvImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: markus
 * Date: 13.10.17
 * Time: 10:49
 */

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('csv:import')
            ->setDescription('import a csv file')
            ->addArgument('path', InputArgument::OPTIONAL ,'file to import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('import csv file...');

        $path = $input->getArgument('path') ?: './web/data.csv';
        if($this->getContainer()->get('ms_csv_import')->importCsv($path) === TRUE){
            $output->writeln('done');
        }
    }
}