<?php
/**
 * Created by PhpStorm.
 * User: markus
 * Date: 13.10.17
 * Time: 10:45
 */

namespace MS\CsvImportBundle\Services;

use Doctrine\ORM\EntityManager;
use MS\CsvImportBundle\Entity\Data;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportService
{
    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * ImportService constructor.
     * @param ContainerInterface $container
     * @param EntityManager $em
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param $filename
     * @param string $delimiter
     * @return bool|mixed
     */
    public function importCsv($filename, $delimiter = ",")
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $this->arrayToTable($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function arrayToTable($data)
    {
        foreach ($data as $key => $value){
            $table = new Data();

            $table->setName($value['name']);
            $table->setNumber($value['number']);

            $this->em->persist($table);
        }

        $this->em->flush();

        return TRUE;
    }
}