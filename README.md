# MSCsvImportBundle #

Small Symfony ( ^3.2 ) bundle for import csv files.

## HowTo : ##

Insert `bin/console import:csv` into the console to import a csv file. 
The file `web/data.csv` is set as default.
You can import your own files with the command `bin/console import:csv FILE`.
Please remember to customize the entity.

## install : ##

### Step 1 ###

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new MS\CsvImportBundle\MSCsvImportBundle(),
        // ...
    );
}
```

### Step 2 ###

```php
// routing.yml

ms_wizard:
    resource: "@MSCsvImportBundle/Resources/config/routing.yml"
    prefix:   /
```

### Step 3 ###

```php
// composer.json

"autoload": {
        "psr-4": {
            "": "src/"
        },
```
	
### Step 4 ###

```php
bin/console assets:install --symlink web
```